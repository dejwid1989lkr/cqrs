<?php
/**
 * Created by PhpStorm.
 * User: Dawid
 * Date: 28.10.2020
 * Time: 18:39
 */

namespace App\Module\Notification\Application\CommandHandler;

use App\Module\Notification\Application\Command\SendNotification;
use App\Module\Notification\Domain\Repository\UserRepository;
use App\Module\Notification\Infrastructure\Chanel\ChanelStrategy;

/**
 * Class SendNotificationHandler
 * @package App\Module\Notification\Application\CommandHandler
 */
class SendNotificationHandler
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * SendNotificationHandler constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param SendNotification $command
     * @throws \Exception
     */
    public function __invoke(SendNotification $command)
    {
        $user = $this->userRepository->findByLogin($command->getLogin());

        if(!$user){
            return;
        }

        foreach ($user->getContactChannels() as $channel){
            $strategy = new ChanelStrategy($channel);
            $chanel = $strategy->getStrategy();
            $chanel->setTo($user->getEmail());
            $chanel->setMessage($command->getMessage());
            $chanel->execute();
        }
    }
}