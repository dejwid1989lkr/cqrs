<?php
/**
 * Created by PhpStorm.
 * User: Dawid
 * Date: 28.10.2020
 * Time: 18:34
 */

namespace App\Module\Notification\Application\Command;

/**
 * Class SendNotification
 * @package App\Module\Notification\Application\Command
 */
class SendNotification
{
    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $message;

    /**
     * SendNotification constructor.
     * @param string $login
     * @param string $message
     */
    public function __construct(string $login, string $message)
    {
        $this->login = $login;
        $this->message = $message;
    }

    /**
     * Return login
     *
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * Return message
     *
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }
}