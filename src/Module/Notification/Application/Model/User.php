<?php
/**
 * Created by PhpStorm.
 * User: Dawid
 * Date: 28.10.2020
 * Time: 11:40
 */

namespace App\Module\Notification\Application\Model;

/**
 * Class User
 * @package App\Module\Notification\Application\Model
 */
class User
{
    /**
     * @var string
     */
    public $login;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $contact_channels;
}