<?php
/**
 * Created by PhpStorm.
 * User: Dawid
 * Date: 28.10.2020
 * Time: 20:20
 */

namespace App\Module\Notification\Infrastructure\CLI;

use App\Module\Notification\Application\Command\SendNotification as SendNotificationCommand;
use App\System\CommandBus;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class SendNotification
 * @package App\Module\Notification\Infrastructure\CLI
 */
class SendNotification extends Command
{
    /**
     * Command name
     *
     * @var string
     */
    protected static $defaultName = 'notification:send';

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * SendNotification constructor.
     * @param CommandBus $commandBus
     */
    public function __construct(CommandBus $commandBus)
    {
        parent::__construct();
        $this->commandBus = $commandBus;
    }

    /**
     * Configure command variables
     */
    public function configure()
    {
        $this
            ->setDescription('Send notification to user')
            ->addArgument('m', InputArgument::REQUIRED, 'Message')
            ->addArgument('u', InputArgument::REQUIRED, 'User login');
    }

    /**
     * Executing notification send
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $io->block('Start sending notification.');
        $this->commandBus->dispatch(new SendNotificationCommand($input->getArgument('u'), $input->getArgument('m')));
        $io->success('Complete sending notification.');
    }
}