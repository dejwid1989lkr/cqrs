<?php
/**
 * Created by PhpStorm.
 * User: Dawid
 * Date: 28.10.2020
 * Time: 14:28
 */

namespace App\Module\Notification\Infrastructure\Repository;

use App\Module\Notification\Domain\Entity\User;
use App\Module\Notification\Domain\Repository\UserRepository;
use App\Module\Notification\Infrastructure\Mapper\UserMapper;

/**
 * Class FileUserRepository
 * @package App\Module\Notification\Infrastructure\Repository
 */
class FileUserRepository implements UserRepository
{
    /**
     * @var string
     */
    private $filePath = '/Database/db.json';

    /**
     * @var array
     */
    private $data = [];

    /**
     * @var UserMapper
     */
    private $userMapper;

    /**
     * FileUserRepository constructor.
     * @param UserMapper $userMapper
     * @throws Exception
     */
    public function __construct(UserMapper $userMapper)
    {
        $this->userMapper = $userMapper;
        $this->setDataFromFile(dirname(__FILE__,5).$this->filePath);
    }

    /**
     * Sets data from file to array
     *
     * @param $path
     */
    private function setDataFromFile($path): void
    {
        try{
            $data = file_get_contents($path);
        }catch (Exception $e){
            throw new Exception('This file is unable to read.' ,$e->getCode(), $e);
        }

        try{
            $json = json_decode($data, true);
        }catch (JsonException $e){
            throw new Exception('This content is unable to decode.' ,$e->getCode(), $e);
        }

        $this->data = $json;
    }

    /**
     * Find all users
     *
     * @return array
     */
    public function findAll(): array
    {
        return array_map([$this->userMapper, 'fromArray'], $this->data);
    }

    /**
     * Find one user by login
     *
     * @param $login
     * @return User|null
     */
    public function findByLogin($login):? User
    {
        $logins = array_column($this->data, 'login');

        if(($key = array_search($login, $logins)) !== false){
            return $this->userMapper->fromArray($this->data[$key]);
        }

        return null;
    }
}