<?php

/**
 * Created by PhpStorm.
 * User: Dawid
 * Date: 28.10.2020
 * Time: 15:03
 */

namespace App\Module\Notification\Infrastructure\Mapper;

use App\Module\Notification\Domain\Entity\User;

/**
 * Class UserMapper
 * @package App\Module\Notification\Infrastructure\Mapper
 */
class UserMapper
{
    /**
     * Mapper user from array to User object
     *
     * @param array $data
     * @return User
     */
    public function fromArray(array $data): User
    {
        return new User(
            $data['login'],
            $data['email'],
            $data['contact_channels'] ? explode(',',$data['contact_channels']) : ['email']
        );
    }
}