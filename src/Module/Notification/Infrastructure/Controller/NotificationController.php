<?php
/**
 * Created by PhpStorm.
 * User: Dawid
 * Date: 28.10.2020
 * Time: 12:12
 */

namespace App\Module\Notification\Infrastructure\Controller;

use App\System\QueryBus;
use App\System\BaseController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Module\Notification\Infrastructure\Repository\FileUserRepository;


/**
 * Class NotificationController
 * @package App\Module\Notification\Infrastructure\Controller
 */
class NotificationController extends AbstractController
{
    /**
     * @Route("/send/{message}/{login}", methods={"GET"})
     *
     * @param FileUserRepository $repository
     * @param QueryBus $queryBus
     * @param Request $request
     * @return ApiResponse
     */
    public function send(FileUserRepository $repository, QueryBus $queryBus, string $message, string $login): JsonResponse
    {

        dd($message,$login);

        dd($repository->findAll());
    }


    /**
     * @Route("/users", methods={"GET"})
     *
     * @param FileUserRepository $repository
     * @return ApiResponse
     */
    public function getUsers(FileUserRepository $repository): JsonResponse
    {
        return $this->json($repository->findAll());
    }
}