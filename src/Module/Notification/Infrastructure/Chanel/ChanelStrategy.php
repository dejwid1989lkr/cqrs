<?php
/**
 * Created by PhpStorm.
 * User: Dawid
 * Date: 28.10.2020
 * Time: 23:12
 */

namespace App\Module\Notification\Infrastructure\Chanel;

use App\Module\Notification\Infrastructure\Chanel\Strategy\ChanelInterface;

/**
 * Class ChanelStrategy
 * @package App\Module\Notification\Infrastructure\Chanel
 */
class ChanelStrategy
{
    /**
     * @var string
     */
    private $channel;

    /**
     * ChanelStrategy constructor.
     * @param string $channel
     */
    public function __construct(string $channel)
    {
        $this->channel = $channel;
    }

    /**
     * Return object strategy
     *
     * @return ChanelInterface
     * @throws \Exception
     */
    public function getStrategy(): ChanelInterface
    {
        return new $this->getClassName();
    }

    /**
     * Returning class name of stategy
     *
     * @return string
     * @throws \Exception
     */
    public function getClassName(): string
    {
        if(!$this->channel) {
            throw new \Exception('Strategy is not defined.');
        }

        $className = 'App\\Module\\Notification\\Infrastructure\\Chanel\\Strategy\\Chanel'.ucfirst($this->channel);

        if(!class_exists($className)) {
            throw new \Exception('Strategy '.$className. ' doesn\'t exist.');
        }

        return $className;
    }
}