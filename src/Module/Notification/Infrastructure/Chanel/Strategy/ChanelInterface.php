<?php
/**
 * Created by PhpStorm.
 * User: Dawid
 * Date: 28.10.2020
 * Time: 23:20
 */

namespace App\Module\Notification\Infrastructure\Chanel\Strategy;

/**
 * Interface ChanelInterface
 * @package App\Module\Notification\Infrastructure\Chanel\Strategy
 */
interface ChanelInterface
{
    /**
     * Execute chanel
     *
     * @throws \Exception
     */
    public function execute(): void;
}