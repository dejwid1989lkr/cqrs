<?php
/**
 * Created by PhpStorm.
 * User: Dawid
 * Date: 28.10.2020
 * Time: 23:19
 */

namespace App\Module\Notification\Infrastructure\Chanel\Strategy;

use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\Transport;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;

/**
 * Class ChanelEmail
 * @package App\Module\Notification\Infrastructure\Chanel\Strategy
 */
class ChanelEmail extends Chanel implements ChanelInterface
{
    /**
     * @var MailerInterface
     */
    private $mailer;

    /**
     * From addresess
     *
     * @var string
     */
    private $from = 'dealercrm@example.com';

    /**
     * ChanelEmail constructor.
     */
    public function __construct()
    {
        $transport = Transport::fromDsn($_ENV['MAILER_DSN']);

        $this->mailer = new Mailer($transport);
    }

    /**
     * Execute chanel
     *
     * @throws \Exception
     */
    public function execute(): void
    {
        $email = (new Email())
            ->from($this->from)
            ->to($this->getTo())
            ->subject($this->getSubject())
            ->text($this->getMessage());
        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            throw new \Exception($e->getMessage());
        }
    }
}