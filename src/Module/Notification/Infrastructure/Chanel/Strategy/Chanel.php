<?php
/**
 * Created by PhpStorm.
 * User: Dawid
 * Date: 28.10.2020
 * Time: 23:20
 */

namespace App\Module\Notification\Infrastructure\Chanel\Strategy;

/**
 * Class Chanel
 * @package App\Module\Notification\Infrastructure\Chanel\Strategy
 */
abstract class Chanel
{
    /**
     * Subject
     *
     * @var string
     */
    protected $subject = 'Notification';

    /**
     * @var string
     */
    private $to;

    /**
     * @var string
     */
    private $message;

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     * @param string $to
     */
    public function setTo(string $to): void
    {
        $this->to = $to;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }
}