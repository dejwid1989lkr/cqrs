<?php
/**
 * Created by PhpStorm.
 * User: Dawid
 * Date: 28.10.2020
 * Time: 23:19
 */

namespace App\Module\Notification\Infrastructure\Chanel\Strategy;

use Symfony\Component\Filesystem\Filesystem;

/**
 * Class ChanelSms
 * @package App\Module\Notification\Infrastructure\Chanel\Strategy
 */
class ChanelSms extends Chanel implements ChanelInterface
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * ChanelSMS constructor.
     */
    public function __construct()
    {
        $this->filesystem = new Filesystem();
    }

    /**
     * Execute chanel
     *
     * @throws \Exception
     */
    public function execute(): void
    {
        $this->filesystem->appendToFile('sms_logs.txt', $this->getContent());
    }

    /**
     * Return formatted content
     *
     * @return string
     */
    private function getContent(): string
    {
        return sprintf("subject: %s\r\nto: %s\r\nmessage: %s\r\n",
            $this->getSubject(),
            $this->getTo(),
            $this->getMessage()
        );
    }
}