<?php
/**
 * Created by PhpStorm.
 * User: Dawid
 * Date: 28.10.2020
 * Time: 11:40
 */

namespace App\Module\Notification\Domain\Entity;

/**
 * Class User
 * @package App\Module\Notification\Application\Model
 */
class User
{
    /**
     * @var string
     */
    public $login;

    /**
     * @var string
     */
    public $email;

    /**
     * @var array
     */
    public $contact_channels = [];

    /**
     * User constructor.
     * @param string $login
     * @param string $email
     * @param array $contact_channels
     */
    public function __construct(string $login, string $email, array $contact_channels)
    {
        $this->login = $login;
        $this->email = $email;
        $this->contact_channels = $contact_channels;
    }

    /**
     * Return login
     *
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * Set login
     *
     * @param string $login
     */
    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    /**
     * Return email
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * Return channels
     *
     * @return array
     */
    public function getContactChannels(): array
    {
        return $this->contact_channels;
    }

    /**
     * Set channels
     *
     * @param array $contact_channels
     */
    public function setContactChannels(array $contact_channels): void
    {
        $this->contact_channels = $contact_channels;
    }
}