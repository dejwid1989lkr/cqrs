<?php
/**
 * Created by PhpStorm.
 * User: Dawid
 * Date: 28.10.2020
 * Time: 14:57
 */

namespace App\Module\Notification\Domain\Repository;

use App\Module\Notification\Domain\Entity\User;

/**
 * Interface UserRepository
 */
interface UserRepository
{
    /**
     * Find all users
     *
     * @return array
     */
    public function findAll(): array;

    /**
     * Find one user by login
     *
     * @param $login
     * @return User|null
     */
    public function findByLogin($login):? User;
}