<?php
/**
 * Created by PhpStorm.
 * User: Dawid
 * Date: 28.10.2020
 * Time: 11:31
 */

namespace App\System;

use App\System\Messenger\TransportStamp;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\StampInterface;

class EventBus implements MessageBusInterface
{

    /**
     * @var MessageBusInterface
     */
    private $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    /**
     * Dispatches the given message.
     *
     * @param object|Envelope $message The message or the message pre-wrapped in an envelope
     * @param StampInterface[] $stamps $stamps
     * @return Envelope
     */
    public function dispatch($message, array $stamps = []): Envelope
    {
        return $this->messageBus->dispatch($message, $stamps);
    }
}