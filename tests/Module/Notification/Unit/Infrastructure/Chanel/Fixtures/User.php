<?php

namespace Tests\Module\Notification\Unit\Infrastructure\Chanel\Fixtures;

class User
{
    public static function getData(): array
    {
        return[
            "login" => "andrzej.kowalski",
            "email" => "andrzej.kowalski@test.pl",
            "contact_channels" => "email,sms"
        ];
    }
}

