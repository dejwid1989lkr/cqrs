<?php
/**
 * Created by PhpStorm.
 * User: Dawid
 * Date: 23.11.2020
 * Time: 16:31
 */
namespace Tests\Module\Notification\Unit\Infrastructure\Chanel;

use PHPUnit\Framework\TestCase;
use App\Module\Notification\Domain\Entity\User;
use App\Module\Notification\Infrastructure\Mapper\UserMapper;
use Tests\Module\Notification\Unit\Infrastructure\Chanel\Fixtures\User as UserEntity;

class UserMapperTest extends TestCase
{
    private $mapperTest;

    public function setUp(): void
    {
        $this->mapperTest = new UserMapper();
    }

    /**
     * @throws \Throwable
     */
    public function testShouldResolveUser()
    {
        // Given
        $mapper = $this->mapperTest;

        $data = UserEntity::getData();

        // When
        $actual = $mapper->fromArray($data);

        // Then
        $this->assertInstanceOf(User::class, $actual);
        $this->assertEquals('andrzej.kowalski', $actual->getLogin());
        $this->assertEquals('andrzej.kowalski@test.pl', $actual->getEmail());
        $this->assertEquals(['email','sms'], $actual->getContactChannels());
    }
}