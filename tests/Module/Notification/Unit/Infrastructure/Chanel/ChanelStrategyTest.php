<?php
/**
 * Created by PhpStorm.
 * User: Dawid
 * Date: 28.10.2020
 * Time: 23:12
 */

namespace Tests\Module\Notification\Unit\Infrastructure\Chanel;

use App\Module\Notification\Infrastructure\Chanel\ChanelStrategy;
use App\Module\Notification\Infrastructure\Chanel\Strategy\ChanelEmail;
use App\Module\Notification\Infrastructure\Chanel\Strategy\ChanelSms;
use PHPUnit\Framework\TestCase;

class ChanelStrategyTest extends TestCase
{
    /**
     * @throws \Exception
     */
    public function testShouldDetectStrategy()
    {
        // Given
        $emalChanel = new ChanelStrategy('email');
        $smsChanel = new ChanelStrategy('sms');
        $emptyChanel = new ChanelStrategy('');

        // Then
        $this->assertEquals(ChanelEmail::class, $emalChanel->getClassName());
        $this->assertEquals(ChanelSms::class, $smsChanel->getClassName());

        $expected = $this->expectException(\Exception::class);
        $this->assertEquals($expected, $emptyChanel->getClassName());
    }
}